extends UIAction

class_name SimpleAction

export var text := "" setget _set_text
export(Texture) var icon setget _set_icon
export(Texture) var untoggled_icon setget _set_untoggled_icon
export(String) var shortcut_action setget _set_shortcut_action
export var disabled := false setget _set_disabled
export var toggleable := false setget _set_toggleable
export var toggled := false setget _set_toggled

#############
# overrides #
#############
func _get_text() -> String:
	return text

func _get_icon() -> Texture:
	if untoggled_icon && !toggled:
		return untoggled_icon
	return icon

func _get_shortcut() -> ShortCut:
	if shortcut_action:
		var action_list = InputMap.get_action_list(shortcut_action)
		if !action_list:
			return null
		var shortcut := ShortCut.new()
		shortcut.shortcut = action_list[0]
		return shortcut
	return null

func _is_disabled() -> bool:
	return disabled

func _is_toggleable() -> bool:
	return toggleable

func _is_toggled() -> bool:
	return toggled

func _toggle(value : bool):
	if toggleable:
		_set_toggled(value)

func _apply():
	emit_signal("applied")

func _update():
	emit_signal("update", self)

###########
# setters #
###########
func _set_text(value : String):
	if value != text:
		text = value
		emit_signal("text_changed")

func _set_icon(value : Texture):
	if value != icon:
		icon = value
		emit_signal("icon_changed")

func _set_untoggled_icon(value : Texture):
	if value != untoggled_icon:
		untoggled_icon = value
		emit_signal("icon_changed")

func _set_shortcut_action(value : String):
	if value != shortcut_action:
		shortcut_action = value
		emit_signal("shortcut_changed")

func _set_disabled(value : bool):
	if value != disabled:
		disabled = value
		emit_signal("disabled_changed")

func _set_toggled(value : bool):
	if value != toggled:
		toggled = value
		emit_signal("toggled_changed")
		if toggled:
			_set_toggleable(true)
		if untoggled_icon:
			emit_signal("icon_changed")

func _set_toggleable(value : bool):
	if value != toggleable:
		toggleable = value
		emit_signal("toggleable_changed")
		if !toggleable:
			_set_toggled(false)

###########
# signals #
###########
signal applied()
signal update(action)
signal toggleable_changed()
