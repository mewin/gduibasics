extends FileAdapter

class_name LocalFileAdapter

var __dir = Directory.new()

class LocalFileEntry:
	extends FileAdapter.FileEntry
	
	var path : String
	var is_link : bool
	var is_folder : bool
	var size := -1
	var modified_time := -1
	
	func _init(path_ : String, is_link_ : bool, is_folder_ : bool):
		path = path_
		is_link = is_link_
		is_folder = is_folder_
		if path.ends_with("/") && path != "/":
			path = path.left(path.length() - 1)
	
	func _is_folder() -> bool:
		return is_folder
	
	func _is_link() -> bool:
		return is_link
	
	func _list_files() -> Array:
		if !is_folder:
			return []
		var dir := Directory.new()
		if dir.open(path + "/") != OK:
			return []
		if dir.list_dir_begin(true) != OK:
			return []
		var fname := dir.get_next()
		var files := []
		while fname:
			var fpath := path.plus_file(fname)
			var ffolder := dir.current_is_dir()
			var flink := false
			
			files.append(LocalFileEntry.new(fpath, flink, ffolder))
			
			fname = dir.get_next()
		return files
	
	func _get_name() -> String:
		return path.get_file()
	
	func _get_size() -> int:
		if size == -1:
			size = __calc_size()
		return size
	
	func _get_modified_time() -> int:
		if modified_time == -1:
			modified_time = __calc_modified_time()
		return modified_time
	
	func _get_parent() -> FileAdapter.FileEntry:
		var parent_path := path.get_base_dir()
		if parent_path && parent_path != path:
			return LocalFileEntry.new(parent_path, false, true)
		return null
	
	func __calc_size() -> int:
		if is_folder:
			return 0
		var file := File.new()
		if file.open(path, File.READ) != OK:
			return 0
		return file.get_len()
	
	func __calc_modified_time() -> int:
		if is_folder:
			return 0
		var file := File.new()
		return file.get_modified_time(path)

func _get_file(path : String) -> FileEntry:
	if OS.has_feature("Windows"): # has "feature"
		path = path.replace("\\", "/")
		if path.length() < 2 || path[1] != ":":
			path = "c:/" + path.lstrip("/")
		if path.length() == 2: # 
			path += "/"
	elif !path.begins_with("/"):
		path = "/" + path # only drive letter, dir_exists() wouldnt work
	
	var dir := Directory.new()
	if dir.dir_exists(path):
		return LocalFileEntry.new(path, false, true)
	elif dir.file_exists(path):
		return LocalFileEntry.new(path, false, false)
	else:
		return null

func _get_root() -> FileAdapter.FileEntry:
	return _get_file("/")

func _get_drives() -> Array:
	var dir := Directory.new()
	var drives := []
	
	for i in range(dir.get_drive_count()):
		drives.append(dir.get_drive(i))
	
	return drives
