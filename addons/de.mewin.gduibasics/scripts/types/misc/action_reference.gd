extends Node

class_name ActionReference

export(NodePath) var action

func get_action() -> UIAction:
	return get_node_or_null(action) as UIAction
