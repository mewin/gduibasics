extends Node

class_name UIAction, "res://addons/de.mewin.gduibasics/images/action.svg"

export var continuous_update = false setget _set_continuous_update

#############
# overrides #
#############
func _ready():
	set_process(continuous_update)

func _process(delta):
	_update()

################
# overridables #
################
func _get_text() -> String:
	return ""

func _get_icon() -> Texture:
	return null

func _get_shortcut() -> ShortCut:
	return null

func _is_disabled() -> bool:
	return false

func _is_toggleable() -> bool:
	return false

func _is_toggled() -> bool:
	return false

func _toggle(value : bool):
	pass

func _apply():
	_toggle(!_is_toggled())

func _update():
	pass

###########
# setters #
###########
func _set_continuous_update(value):
	if value != continuous_update:
		continuous_update = value
		set_process(continuous_update)

###########
# signals #
###########
signal text_changed()
signal icon_changed()
signal shortcut_changed()
signal disabled_changed()
signal toggled_changed()
