tool
extends Node

class_name DynamicGridConstraints

export(int, 1, 100) var colspan := 1 setget _set_colspan
export(int, 1, 100) var rowspan := 1 setget _set_rowspan

func _ready():
	__trigger_update()

func __trigger_update():
	var container := get_node_or_null("../..") as Container # cyclic inclusion, cannot use DynamicGridContainer
	if container != null:
		container.notification(Container.NOTIFICATION_SORT_CHILDREN)

func _set_colspan(value : int):
	if value != colspan:
		colspan = value
		__trigger_update()

func _set_rowspan(value : int):
	if value != rowspan:
		rowspan = value
		__trigger_update()
