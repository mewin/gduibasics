extends FileFilter

class_name FileExtensionFilter

var __extensions : PoolStringArray

func _init(extensions_ : Array):
	for ext in extensions_:
		if !ext.begins_with("."):
			ext = "." + ext
		__extensions.append(ext)

func _accepts(entry) -> bool:
	for ext in __extensions:
		if entry._get_name().ends_with(ext):
			return true
	return false

func _get_default_name() -> String:
	return tr("%s-Files") % __extensions.join(", ")
