extends Reference

class_name FileFilter

var name := ""

func _accepts(entry) -> bool:
	return true

func _get_name() -> String:
	if name:
		return name
	return _get_default_name()

func _get_default_name() -> String:
	return ""
