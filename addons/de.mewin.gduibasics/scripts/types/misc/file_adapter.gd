extends Reference

class_name FileAdapter

class FileEntry:
	func _is_folder() -> bool:
		return false
	
	func _is_link() -> bool:
		return false
	
	func _list_files() -> Array:
		return []
	
	func _get_name() -> String:
		return ""
	
	func _get_size() -> int:
		return 0
	
	func _get_modified_time() -> int:
		return 0
	
	func _get_parent() -> FileEntry:
		return null
	
	func _get_path() -> String:
		var parent := _get_parent()
		if parent:
			return parent._get_path().plus_file(_get_name())
		var name := _get_name()
		if name == "":
			return "/"
		return name

func _get_root() -> FileEntry:
	return null

func _get_file(path : String) -> FileEntry:
	return null

func _get_icon(entry) -> Texture:
	if entry._is_folder():
		return preload("res://addons/de.mewin.gduibasics/images/folder.svg")
	else:

		return preload("res://addons/de.mewin.gduibasics/images/file.svg")

func _get_drives() -> Array:
	return [""]
