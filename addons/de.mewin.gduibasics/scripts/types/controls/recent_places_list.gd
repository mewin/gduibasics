extends PlacesList

var settings = G.import("settings")

#############
# overrides #
#############
func _ready():
	places = settings.get_value(C.SETTING_RECENT_PLACES, [])
	
	settings.connect("setting_changed", self, "_on_setting_changed")

############
# handlers #
############
func _on_setting_changed(setting, value):
	if setting == C.SETTING_RECENT_PLACES:
		_set_places(value)
