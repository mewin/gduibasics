extends BoxContainer

class_name ActionToolbar

#############
# overrides #
#############
func _ready():
	for child in get_children():
		if child is UIAction:
			add_action(child)
		elif child is ActionReference:
			add_action(child.get_action())

################
# public stuff #
################
func add_action(action : UIAction):
	var btn = ToolButton.new()
	btn.set_script(preload("res://addons/de.mewin.gduibasics/scripts/types/controls/action_button.gd"))
	btn.action = action
	call_deferred("add_child", btn)

func remove_action(action : UIAction):
	for child in get_children():
		if child == action || child.get("action") == action \
				|| (child is ActionReference && child.get_action() == action):
			remove_child(child)
			child.queue_free()
