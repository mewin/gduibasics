extends BorderContainer

class_name WindowBorderContainer

export var minimum_size := Vector2(100.0, 100.0)

#############
# overrides #
#############
func _win_get_position() -> Vector2:
	return OS.window_position

func _win_get_size() -> Vector2:
	return OS.window_size

func _win_get_mouse_position() -> Vector2:
	return get_viewport().get_mouse_position() + OS.window_position

func _win_get_minimum_size() -> Vector2:
	return minimum_size

func _win_set_position(pos : Vector2):
	if pos != OS.window_position:
		OS.window_position = pos
		OS.window_maximized = false

func _win_set_size(size : Vector2):
	OS.window_size = size
