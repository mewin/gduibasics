extends Button

class_name ActionButton

var util = G.import("util")

var action : UIAction setget _set_action

#############
# overrides #
#############
func _ready():
	if !action:
		_set_action(util.find_node_by_type(self, UIAction))
	if !action:
		var action_reference : ActionReference = util.find_node_by_type(self, ActionReference)
		if action_reference:
			_set_action(action_reference.get_action())

func _pressed():
	if action:
		action._apply()

#################
# private stuff #
#################
func __connect():
	if !action:
		return
	action.connect("disabled_changed", self, "_on_action_disabled_changed")
	action.connect("toggled_changed", self, "_on_action_toggled_changed")
	action.connect("text_changed", self, "_on_action_text_changed")
	action.connect("icon_changed", self, "_on_action_icon_changed")

func __disconnect():
	if !action:
		return
	action.disconnect("disabled_changed", self, "_on_action_disabled_changed")
	action.disconnect("toggled_changed", self, "_on_action_toggled_changed")
	action.disconnect("text_changed", self, "_on_action_text_changed")
	action.disconnect("icon_changed", self, "_on_action_icon_changed")

############
# handlers #
############
func _on_action_disabled_changed():
	disabled = action._is_disabled()

func _on_action_toggled_changed():
	toggle_mode = action._is_toggled()
	pressed = action._is_toggled()

func _on_action_text_changed():
	if !icon:
		text = action._get_text()
	hint_tooltip = action._get_text()

func _on_action_icon_changed():
	icon = action._get_icon()
	_on_action_text_changed() # update hint/text

###########
# setters #
###########
func _set_action(value : UIAction):
	if value != action:
		__disconnect()
		action = value
		if action:
			icon = action._get_icon()
			if !icon:
				text = action._get_text()
			hint_tooltip = action._get_text()
			disabled = action._is_disabled()
			toggle_mode = action._is_toggled()
			pressed = action._is_toggled()
			__connect()
		else:
			icon = null
			text = ""
			hint_tooltip = ""
			disabled = true
			toggle_mode = false
			pressed = false
