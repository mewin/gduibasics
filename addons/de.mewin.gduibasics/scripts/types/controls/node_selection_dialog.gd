tool
extends ConfirmationDialog

func get_editor_node_tree() -> EditorNodeTree:
	return $tree as EditorNodeTree
