extends ItemList

class_name TabsItemList

export(NodePath) var tab_container

#############
# overrides #
#############
func _ready():
	call_deferred("__setup")
	connect("item_selected", self, "_on_item_selected")

#################
# private stuff #
#################
func __get_tab_container() -> TabContainer:
	return get_node_or_null(tab_container) as TabContainer

func __setup():
	var tab_container_ := __get_tab_container()
	clear()
	
	if !tab_container_:
		return
	
	for i in range(tab_container_.get_child_count()):
		var title := tab_container_.get_tab_title(i)
		var icon := tab_container_.get_tab_icon(i)
		add_item(title, icon)
	
	select(tab_container_.current_tab)

############
# handlers #
############
func _on_item_selected(idx : int):
	var tab_container_ := __get_tab_container()

	if tab_container_:
		tab_container_.current_tab = idx
