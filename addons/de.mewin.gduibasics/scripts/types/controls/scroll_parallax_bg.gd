extends Control

class_name ScrollParallaxBackground

export(Array, Texture) var textures := []
export(Array, float) var speeds := []
export(NodePath) var scroll_container

var __scroll_container : ScrollContainer

func _ready():
	show_behind_parent = true
	rect_clip_content = true
	
	if !scroll_container:
		for child in get_children():
			if child is ScrollContainer:
				scroll_container = get_path_to(child)
				break
	
	__scroll_container = get_node_or_null(scroll_container) as ScrollContainer
	
	set_process(false)

	if __scroll_container:
#		__scroll_container.connect("scroll_started", self, "_on_scroll_started")
#		__scroll_container.connect("scroll_ended", self, "_on_scroll_ended")
		__scroll_container.get_h_scrollbar().connect("value_changed", self, "_on_scroll_value_changed")
		__scroll_container.get_v_scrollbar().connect("value_changed", self, "_on_scroll_value_changed")

func _draw():
	if !__scroll_container || __scroll_container.get_child_count() < 1:
		return
	var first_child := __scroll_container.get_child(0) as Control
	if !first_child:
		return
	
	for i in range(textures.size()):
		var texture : Texture = textures[i]
		var speed : float = speeds[i] if i < speeds.size() else 1.0
		var rel_scroll := __scroll_container.scroll_vertical / max(first_child.rect_size.y - __scroll_container.rect_size.y, 1.0)
		var tex_size := rect_size
		var tex_pos := Vector2()
		tex_size.y = tex_size.x * (texture.get_height() / texture.get_width())
		tex_pos.y = speed * -rel_scroll * (tex_size.y - rect_size.y)
		draw_texture_rect(texture, Rect2(tex_pos, tex_size), false)

func _process(delta):
	update()

func _on_scroll_started():
	set_process(true)

func _on_scroll_ended():
	set_process(false)

func _on_scroll_value_changed(value : float):
	update()
