tool
extends ConfirmationDialog

class_name GDUILineInputDialog

export(bool) var allow_empty := true setget _set_allow_empty

################
# public stuff #
################
func get_line_edit() -> LineEdit:
	return $container/line_edit as LineEdit

#################
# private stuff #
#################
func __update():
	var valid := true
	if !allow_empty && !$container/line_edit.text:
		valid = false
	get_ok().disabled = !valid

###########
# setters #
###########
func _set_allow_empty(value : bool):
	if value != allow_empty:
		allow_empty = value
		__update()

############
# handlers #
############
func _on_line_input_dialog_about_to_show():
	__update()

func _on_line_edit_text_changed(new_text : String):
	__update()

func _on_line_edit_text_entered(new_text):
	if !get_ok().disabled:
		visible = false
		emit_signal("confirmed")

func _on_line_input_dialog_visibility_changed():
	if visible:
		yield(get_tree(), "idle_frame")
		$container/line_edit.grab_focus()
		$container/line_edit.caret_position = $container/line_edit.text.length()
