extends LineEdit

class_name FolderEdit

var suggestion_popup := SuggestionPopup.new()
var adapter : FileAdapter
var current_folder : FileAdapter.FileEntry setget _set_current_folder

#############
# overrides #
#############
func _ready():
	call_deferred("__setup")
	connect("text_changed", self, "_on_text_changed")
	connect("text_entered", self, "_on_text_entered")

#################
# private stuff #
#################
func __setup():
	suggestion_popup.connect("suggestion_accepted", self, "_on_suggestion_accepted")
	add_child(suggestion_popup)

func __set_suggestions(files : Array):
	var suggestions := []
	
	for file in files:
		if file._is_folder():
			suggestions.append(file._get_path() + "/")
	
	suggestion_popup.suggestions = suggestions
	
func __update_suggestions():
	var folder = adapter._get_file(text)
	
	if folder && folder._is_folder():
		__set_suggestions(folder._list_files())
		return
	var parent_path = text.get_base_dir()
	
	if parent_path:
		folder = adapter._get_file(parent_path)
		if folder && folder._is_folder():
			__set_suggestions(folder._list_files())
			return
	suggestion_popup.suggestions = []

func __update_current_folder():
	var new_file = adapter._get_file(text)
	if new_file:
		_set_current_folder(new_file)
		caret_position = text.length()

###########
# setters #
###########
func _set_current_folder(value):
	if value != current_folder:
		current_folder = value
		text = current_folder._get_path()
		emit_signal("current_folder_changed")

############
# handlers #
############
func _on_text_changed(new_text):
	__update_suggestions()

func _on_text_entered(new_text):
	__update_current_folder()

func _on_suggestion_accepted(suggestion):
	__update_current_folder()
	__update_suggestions()

###########
# signals #
###########
signal current_folder_changed()
