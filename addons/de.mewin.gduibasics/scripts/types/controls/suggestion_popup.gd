extends Popup

class_name SuggestionPopup

export(NodePath) var line_edit_path = ".."
export(int, 1, 20) var max_values = 5
export(Array, String) var suggestions = [] setget _set_suggestions
export(StyleBox) var style_normal = preload("res://addons/de.mewin.gduibasics/styles/suggestion_line_normal.stylebox")
export(StyleBox) var style_active = preload("res://addons/de.mewin.gduibasics/styles/suggestion_line_active.stylebox")
onready var line_edit : LineEdit = get_node(line_edit_path)

var __v_box_container := VBoxContainer.new()
var __panel := Panel.new()
var __labels := []
var __filtered_suggestions := []
var __filter_text := ""
var __active_label := -1
var __scroll := 0
var __up_direction := false

#############
# overrides #
#############
func _ready():
	call_deferred("__initial_setup")

func _gui_input(event : InputEvent):
	if !event.is_pressed() || !event is InputEventMouseButton:
		return
	
	if event.button_index == BUTTON_LEFT:
		__accept_suggestion()
		accept_event()
	elif event.button_index == BUTTON_WHEEL_DOWN:
		if !__up_direction:
			__focus_down()
		else:
			__focus_up()
	elif event.button_index == BUTTON_WHEEL_UP:
		if !__up_direction:
			__focus_up()
		else:
			__focus_down()

#################
# private stuff #
#################
func __initial_setup():
	__panel.show_behind_parent = true
	add_child(__panel)
	
	add_child(__v_box_container)
	
	for i in range(max_values):
		var label := Label.new()
		label.size_flags_horizontal |= Control.SIZE_EXPAND
		label.mouse_filter = Control.MOUSE_FILTER_PASS
		label.connect("mouse_entered", self, "_on_label_mouse_entered", [i])
		
		__labels.append(label)
		__v_box_container.add_child(label)
	__setup()

func __setup():
	if !line_edit:
		return
	line_edit.focus_neighbour_top = "."
	line_edit.focus_neighbour_bottom = "."
	line_edit.connect("focus_entered", self, "_on_line_edit_focus_entered")
	line_edit.connect("focus_exited", self, "_on_line_edit_focus_exited")
	line_edit.connect("text_changed", self, "_on_line_edit_text_changed")
	line_edit.connect("gui_input", self, "_on_line_edit_gui_input")

func __calc_rect() -> Rect2:
	var size := Vector2(line_edit.rect_size.x, __v_box_container.get_combined_minimum_size().y)
	var lower_rect := Rect2(line_edit.rect_global_position \
			+ Vector2(0.0, line_edit.rect_size.y), size)
	if get_viewport_rect().encloses(lower_rect):
		if __up_direction:
			__up_direction = false
			__update_labels(false)
		return lower_rect
	else:
		var upper_rect := Rect2(line_edit.rect_global_position \
				- Vector2(0.0, size.y), size)
		if !__up_direction:
			__up_direction = true
			__update_labels(false)
		return upper_rect

func __get_label(idx : int):
	assert(idx < __labels.size())
	if __up_direction:
		return __labels[__labels.size() - 1 - idx]
	return __labels[idx]

func __score_suggestion(suggestion : String) -> float:
	if suggestion.to_lower().begins_with(__filter_text):
		return 0.5 + 0.5 * suggestion.similarity(line_edit.text)
	else:
		return 0.5 * suggestion.similarity(line_edit.text)

func __compare_suggestions(sugg0 : String, sugg1 : String):
	var score0 := __score_suggestion(sugg0)
	var score1 := __score_suggestion(sugg1)
	if score0 != score1:
		return score0 > score1
	else:
		return sugg0 < sugg1

func __filter():
	__filter_text = line_edit.text.to_lower() # .left(line_edit.caret_position).to_lower()
	__filtered_suggestions = suggestions.duplicate()
	__filtered_suggestions.sort_custom(self, "__compare_suggestions")

func __update_labels(update_size := true):
	for i in range(__labels.size()):
		var label : Label = __get_label(i)
		if i >= __filtered_suggestions.size():
			label.visible = false
		else:
			label.visible = true
			label.text = __filtered_suggestions[(i + __scroll) % __filtered_suggestions.size()]
		if i == __active_label:
			label.add_stylebox_override("normal", style_active)
		else:
			label.add_stylebox_override("normal", style_normal)
	
	if update_size:
		var rect := __calc_rect()
		rect_position = rect.position
		rect_size = rect.size
		__panel.rect_size = rect_size
		__v_box_container.rect_size = rect_size

func __focus_down():
	if __active_label == __labels.size() - 1:
		__scroll = min(__scroll + 1, __filtered_suggestions.size() - __labels.size())
		if __scroll < 0:
			__scroll = 0
	else:
		__active_label += 1
	__update_labels()
	line_edit.caret_position = line_edit.text.length()

func __focus_up():
	if __active_label < 0:
		return
	elif __active_label > 0:
		__active_label -= 1
	elif __scroll > 0:
		__scroll -= 1
	else:
		__active_label = -1
	__update_labels()
	line_edit.caret_position = line_edit.text.length()

func __accept_suggestion():
	if __active_label < 0:
		return
	var suggestion = __get_label(__active_label).text
	line_edit.text = suggestion
	line_edit.caret_position = suggestion.length()
	__active_label = -1
	__scroll = 0
	__filter()
	__update_labels()
	emit_signal("suggestion_accepted", suggestion)

###########
# setters #
###########
func _set_suggestions(value : Array):
	suggestions = value
	if visible:
		__filter()
		__update_labels()

############
# handlers #
############
func _on_line_edit_focus_entered():
	__filter()
	__update_labels()
	
	popup()

func _on_line_edit_focus_exited():
	visible = false

func _on_line_edit_text_changed(new_text : String):
	if !line_edit || !line_edit.has_focus():
		return
	
	__active_label = -1
	__scroll = 0
	
	__filter()
	__update_labels()
	if !visible:
		popup()

func _on_line_edit_gui_input(input_event : InputEvent):
	if !input_event.is_pressed():
		return
	
	if input_event.is_action_pressed("ui_accept"):
		__accept_suggestion()
	elif input_event.is_action("ui_down"):
		if !__up_direction:
			__focus_down()
		else:
			__focus_up()
	elif input_event.is_action("ui_up"):
		if !__up_direction:
			__focus_up()
		else:
			__focus_down()

func _on_label_mouse_entered(index : int):
	if __up_direction:
		index = __labels.size() - index - 1
	
	if __active_label != index:
		__active_label = index
		__update_labels()

###########
# signals #
###########
signal suggestion_accepted(suggestion)
