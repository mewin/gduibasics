extends ItemList

class_name PlacesList

var fsutil = G.import("fsutil")
var ux = G.import("ux")

enum Mode {
	DEFAULT,
#	FAVOURITES,
	CUSTOM
}

class __DefaultItem:
	var place : String
	var icon : Texture
	var label : String
	
	func _init(place_ : String, icon_ : Texture, label_ : String):
		place = place_
		icon = icon_
		label = label_

var __DEFAULT_ITEMS := [
	__DefaultItem.new(fsutil.get_home_folder(), preload("res://addons/de.mewin.gduibasics/images/home32.svg"), tr("Home"))
]

export(Mode) var mode = Mode.DEFAULT setget _set_mode
export(Array, String) var places := [] setget _set_places
export(Array, Texture) var icons := []
export(Array, String) var names := []

var adapter : FileAdapter
var current_folder : FileAdapter.FileEntry setget _set_current_folder

#############
# overrides #
#############
func _ready():
	call_deferred("__update_items")
	
	connect("item_activated", self, "_on_item_activated")
	ux.connect("favourite_places_changed", self, "_on_favourite_places_changed")

#################
# private stuff #
#################
func __update_items():
	clear()
	if !adapter:
		return
	
	match mode:
		Mode.DEFAULT:
			__add_default_places()
		_:
			__add_custom_places()

func __add_default_places():
	for default_place in __DEFAULT_ITEMS:
		var folder := adapter._get_file(default_place.place)
		if !folder || !folder._is_folder():
			continue
		__add_place(folder, default_place.icon, default_place.label)
	
	for favourite in ux.get_favourite_places():
		var folder := adapter._get_file(favourite)
		if !folder || !folder._is_folder():
			continue
		__add_place(folder, preload("res://addons/de.mewin.gduibasics/images/favourite32.svg"))

func __add_custom_places():
	for i in range(places.size()):
		var folder = adapter._get_file(places[i])
		if !folder || !folder._is_folder():
			continue
		var label : String = names[i] if i < names.size() else ""
		var icon : Texture = icons[i] if i < icons.size() else null
		__add_place(folder, icon, label)

func __add_place(folder : FileAdapter.FileEntry, icon : Texture, label := ""):
	if !icon:
		icon = preload("res://addons/de.mewin.gduibasics/images/folder32.svg")
	if !label:
		label = folder._get_name()
	var idx = get_item_count()
	add_item(label, icon)
	set_item_metadata(idx, folder)

func __select_current_folder():
	for i in range(get_item_count()):
		if get_item_metadata(i)._get_path() == current_folder._get_path():
			select(i)

###########
# setters #
###########
func _set_mode(value):
	if value != mode:
		mode = value
		__update_items()

func _set_places(value):
	if value != places:
		places = value
		__update_items()

func _set_current_folder(value):
	if value != current_folder:
		current_folder = value
		emit_signal("current_folder_changed")

############
# handlers #
############
func _on_item_activated(idx):
	_set_current_folder(get_item_metadata(idx))

func _on_favourite_places_changed():
	if mode == Mode.DEFAULT:
		__update_items()
###########
# signals #
###########
signal current_folder_changed()
