tool
extends Control

onready var line_edit := $line_edit
onready var button := $button
onready var node_selection_dialog := $node_selection_dialog

export(NodePath) var selected_node setget _set_selected_node
var node_type_filter = Node

func _ready():
	line_edit.text = str(selected_node)

func _set_selected_node(value : NodePath):
	if value != selected_node:
		selected_node = value
		if line_edit:
			line_edit.text = str(selected_node)

func _on_button_pressed():
	node_selection_dialog.get_editor_node_tree().node_type_filter = node_type_filter
	node_selection_dialog.get_editor_node_tree().selected_node = selected_node
	node_selection_dialog.popup_centered_ratio(0.5)

func _on_node_selection_dialog_confirmed():
	var tree = node_selection_dialog.get_editor_node_tree()
	if tree.selected_node != selected_node:
		_set_selected_node(tree.selected_node)
		emit_signal("selected_node_changed")

signal selected_node_changed()
