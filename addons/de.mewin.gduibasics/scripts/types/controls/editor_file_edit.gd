tool
extends Control

onready var line_edit := $line_edit
onready var button := $button
onready var file_dialog := $file_dialog

export(String, FILE) var selected_file setget _set_selected_file
var file_filter = ""

func _ready():
	line_edit.text = selected_file

func _set_selected_file(value : String):
	if value != selected_file:
		selected_file = value
		if line_edit:
			line_edit.text = selected_file

func _on_button_pressed():
	file_dialog.clear_filters()
	file_dialog.add_filter(file_filter)
	file_dialog.filename = selected_file
	file_dialog.popup_centered_ratio(0.5)

func _on_file_dialog_file_selected(path : String):
	if path != selected_file:
		_set_selected_file(path)
		emit_signal("selected_file_changed")

signal selected_file_changed()
