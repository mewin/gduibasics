tool
extends Container

class_name DynamicGridContainer

var __DEFAULT_CONSTRAINTS = DynamicGridConstraints.new()

export(int, 1, 100) var columns := 1 setget _set_columns

func _notification(what):
	if what == NOTIFICATION_SORT_CHILDREN:
		__sort_children()

func __get_constraints(node : Node) -> DynamicGridConstraints:
	for child in node.get_children():
		if child is DynamicGridConstraints:
			return child
	return __DEFAULT_CONSTRAINTS

func __sort_children():
	var column_widths := []
	var column_expand := []
	
	for i in range(columns):
		column_widths.append(0)
		column_expand.append(0.0)
	
	var rows := [[]]
	var col := 0
	var row_width := 0.0
	
	for child in get_children():
		if !child is Control || !child.visible:
			continue
		var constraints := __get_constraints(child)
		
		if col + constraints.colspan > columns:
			col = 0
			row_width = 0.0
			rows.append([])
		
		rows[rows.size() - 1].append(child)
		
		child.rect_size = child.get_combined_minimum_size()
		column_widths[col] = max(column_widths[col], child.rect_size.x)
		
		if child.size_flags_horizontal & Control.SIZE_EXPAND:
			column_expand[col] = max(column_expand[col], child.size_flags_stretch_ratio)
		row_width += child.rect_size.x
		
		col = col + constraints.colspan
	
	var full_width := 0.0
	for col_width in column_widths:
		full_width += col_width
	var remaining_width := rect_size.x - full_width
	if remaining_width > 0.0:
		var combined_expand_ratio := 0.0
		for expand in column_expand:
			combined_expand_ratio += expand
		if combined_expand_ratio > 0.0:
			for c in range(columns):
				column_widths[c] += remaining_width * (column_expand[c] / combined_expand_ratio)
	
	var pos := Vector2()
	for row in rows:
		var row_height := 0.0
		col = 0
		for child in row:
			var constraints := __get_constraints(child)
			child.rect_position = pos
			
			var width := 0.0
			for i in range(min(constraints.colspan, columns - col)):
				width += column_widths[col + i]
			
			if child.size_flags_horizontal & Control.SIZE_FILL:
				child.rect_size.x = width
			pos.x += width
			
			row_height = max(row_height, child.rect_size.y)
			col = col + constraints.colspan
		
		for child in row:
			if child.size_flags_vertical & Control.SIZE_FILL:
				child.rect_size.y = row_height
		
		pos.y += row_height
		pos.x = 0.0
	rect_min_size.y = pos.y
	
func _set_columns(value : int):
	if value != columns:
		columns = value
		__sort_children()
