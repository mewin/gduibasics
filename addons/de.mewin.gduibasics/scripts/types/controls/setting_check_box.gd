extends CheckBox

class_name SettingCheckBox

var settings = G.import("settings")

export var setting_name = ""

func _ready():
	pressed = settings.get_value(setting_name, false)

func _pressed():
	settings.set_value(setting_name, pressed)
