tool
extends Tree

class_name EditorNodeTree

var __tree : Tree
var selected_node : NodePath setget _set_selected_node
var node_type_filter = Node

export var show_properties := false
export var show_methods := false

#############
# overrides #
#############
func _ready():
	assert(Engine.editor_hint)
	update()
	connect("visibility_changed", self, "_on_visiblity_changed")
	connect("item_selected", self, "_on_item_selected")

################
# public stuff #
################
func update():
	if !__tree:
		var scene_tree_editor := GDBUIUitility.find_scene_tree_editor()
		if !scene_tree_editor:
			return
		__tree = __find_tree(scene_tree_editor)
		if !__tree:
			return
	__copy_from(__tree)
	if !selected_node.is_empty():
		var full_path := __full_path(selected_node)
		if !full_path.is_empty():
			__set_selected_node(full_path, get_root())

#################
# private stuff #
#################
func __copy_from(tree : Tree):
	clear()
	columns = tree.columns
	__copy_items(tree.get_root())

func __copy_items(item : TreeItem, parent : TreeItem = null):
	if !item:
		return
	
	var new_item := create_item(parent)
	for i in range(columns):
		new_item.set_text(i, item.get_text(i))
		new_item.set_icon(i, item.get_icon(i))
		new_item.set_metadata(i, item.get_metadata(i))
	var meta0 = item.get_metadata(0)
	if meta0 is NodePath:
		if show_properties:
			var prop_item := create_item(new_item)
			prop_item.set_text(0, tr("Properties"))
			__fill_node_path_properties(prop_item, meta0)
			prop_item.collapsed = true
			prop_item.set_selectable(0, false)
		if show_methods:
			var method_item := create_item(new_item)
			method_item.set_text(0, tr("Methods"))
			__fill_methods(method_item, meta0)
			method_item.collapsed = true
			method_item.set_selectable(0, false)
		var node : Node = get_node(meta0)
		if !node is node_type_filter:
			new_item.set_selectable(0, false)
	else:
		new_item.set_selectable(0, false)
	if !new_item.is_selectable(0):
		new_item.set_custom_color(0, Color.darkgray)
	__copy_items(item.get_children(), new_item)
	__copy_items(item.get_next(), parent)

func __fill_node_path_properties(parent : TreeItem, node_path : NodePath):
	assert(node_path.is_absolute())
	var node := get_node_or_null(node_path)
	if !node:
		return
	__fill_properties(parent, node.get_property_list(), node_path)

func __fill_properties(parent : TreeItem, properties : Array, parent_path : NodePath):
	GDBAlgorithm.remove_if(properties, 'return prop.get("usage", PROPERTY_USAGE_EDITOR) & PROPERTY_USAGE_EDITOR == 0', ["prop"])
	GDBAlgorithm.sort_by(properties, "name")
	for prop in properties:
		var prop_item := create_item(parent)
		var node_path := NodePath(str(parent_path) + ":" + prop["name"])
		prop_item.set_text(0, "%s : %s" % [prop["name"], GDBUtility.format_type(prop, "any")])
		prop_item.set_metadata(0, node_path)
		__fill_properties(prop_item, GDBUtility.get_type_property_list(prop), node_path)
		prop_item.collapsed = true

func __fill_methods(parent : TreeItem, node_path : NodePath):
	assert(node_path.is_absolute())
	var node := get_node_or_null(node_path)
	if !node:
		return
	var methods := node.get_method_list()
	GDBAlgorithm.sort_by(methods, "name")
	for method in methods:
		var method_item := create_item(parent)
		method_item.set_text(0, GDBUtility.format_method_signature(method))

func __find_tree(root : Node) -> Tree:
	if root is Tree:
		return root as Tree
	for child in root.get_children():
		var tree := __find_tree(child)
		if tree:
			return tree
	return null

func __set_selected_node(node : NodePath, root : TreeItem):
	if !root:
		return false
	for i in range(columns):
		if root.get_metadata(i) == node:
			GDBUIUitility.uncollapse_tree_item(root)
			root.select(i)
			return true
	if __set_selected_node(node, root.get_children()):
		return true
	else:
		return __set_selected_node(node, root.get_next())

func __full_path(rel_path : NodePath) -> NodePath:
	assert(!rel_path.is_absolute())
	var node := get_tree().edited_scene_root.get_node_or_null(rel_path)
	if !node:
		# printerr("EditorNodeTree: could not find node: %s" % rel_path)
		return NodePath()
	var res_path := node.get_path()
	if rel_path.get_subname_count() > 0:
		res_path = NodePath(str(res_path) + ":" + rel_path.get_concatenated_subnames())
	return res_path

func __format_tree_item(tree : Tree, item : TreeItem) -> String:
	var parts := PoolStringArray()
	for i in range(tree.columns):
		parts.append("%s[%s]" % [item.get_text(i), item.get_metadata(i)])
	return parts.join("|")
	
func __dump_tree_item(tree : Tree, item : TreeItem, indent := 0):
	if !item:
		return
	print(" ".repeat(indent), __format_tree_item(tree, item))
	__dump_tree_item(tree, item.get_children(), indent + 2)
	__dump_tree_item(tree, item.get_next(), indent)

func _set_selected_node(value : NodePath):
	if value == selected_node:
		return
	if value.is_empty():
		var selected_item := get_selected()
		if selected_item:
			selected_item.deselect(0)
		return
	
	if value.is_absolute():
		printerr("EditorNodeTree: trying to set absolute NodePath, aborting.")
		return
	
	var full_path := __full_path(value)
	if full_path.is_empty():
		return
	__set_selected_node(full_path, get_root())
	selected_node = value

############
# handlers #
############
func _on_visiblity_changed():
	if visible:
		update()

func _on_item_selected():
	var item := get_selected()
	if !item:
		selected_node = NodePath()
		emit_signal("selected_node_changed")
		return
	for i in range(columns):
		var meta = item.get_metadata(i)
		if meta is NodePath:
			assert(meta.is_absolute())
			var node : Node = get_node(meta)
			if !node:
				printerr("EditorNodeTree contains invalid node path: %s" % meta)
				return
			selected_node = NodePath(str(get_tree().edited_scene_root.get_path_to(node)) + ":" + meta.get_concatenated_subnames())
			emit_signal("selected_node_changed")
			return
	printerr("EditorNodeTree: selected node without NodePath?!")

###########
# signals #
###########
signal selected_node_changed()
