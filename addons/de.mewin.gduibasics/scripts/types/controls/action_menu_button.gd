extends MenuButton

class_name ActionMenuButton

onready var __popup := get_popup()

#############
# overrides #
#############
func _ready():
	for child in get_children():
		if child is UIAction:
			add_action(child)
		elif child is ActionReference:
			add_action(child.get_action())
	
	connect("about_to_show", self, "_on_about_to_show")
	__popup.connect("index_pressed", self, "_on_popup_index_pressed")

################
# public stuff #
################
func add_action(action : UIAction):
	if !action:
		return
	
	var idx = __popup.get_item_count()
	__popup.add_item(action._get_text())
	__popup.set_item_metadata(idx, action)

############
# handlers #
############
func _on_about_to_show():
	for i in range(__popup.get_item_count()):
		var action := __popup.get_item_metadata(i) as UIAction
		if !action:
			continue
		
		action._update()
		__popup.set_item_text(i, action._get_text())
		__popup.set_item_icon(i, action._get_icon())
		__popup.set_item_shortcut(i, action._get_shortcut())
		__popup.set_item_disabled(i, action._is_disabled())
		__popup.set_item_as_checkable(i, action._is_toggleable())
		__popup.set_item_checked(i, action._is_toggled())

func _on_popup_index_pressed(index : int):
	var action := __popup.get_item_metadata(index) as UIAction
	if !action:
		return
	action._apply()
