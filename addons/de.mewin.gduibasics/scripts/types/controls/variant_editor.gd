tool
extends Control

enum Type {
	STRING,
	INT,
	REAL
}

onready var edt_text := $edt_text
onready var spin_number := $spin_number

export var value = "" setget _set_value
export(Type) var type = Type.STRING setget _set_type

func _ready():
	__setup_type()

func __setup_type():
	match type:
		Type.STRING:
			edt_text.visible = true
		Type.INT, Type.REAL:
			spin_number.visible = true
	_set_value(convert(value, __type_id()))
	__fill_value()
	emit_signal("value_changed")

func __fill_value():
	match type:
		Type.STRING:
			edt_text.text = str(value)
		Type.INT, Type.REAL:
			spin_number.value = float(value)

func __type_id() -> int:
	match type:
		Type.STRING:
			return TYPE_STRING
		Type.INT:
			return TYPE_INT
		Type.REAL:
			return TYPE_REAL
		_:
			assert(false)
			return TYPE_STRING

func _set_value(val):
	val = convert(val, __type_id())
	if typeof(value) != __type_id() || value != val:
		value = val
		__fill_value()

func _set_type(value : int):
	if value != type:
		type = value
		for child in get_children():
			child.visible = false
		__setup_type()

func _on_edt_text_text_changed(new_text : String):
	assert(type == Type.STRING)
	if !value is String || value != new_text:
		value = new_text
		emit_signal("value_changed")

func _on_spin_number_value_changed(value_ : float):
	assert(type == Type.INT || type == Type.REAL)
	if type == Type.INT:
		value_ = int(value_)
	if typeof(value) != __type_id() || value != value_:
		value = value_
		emit_signal("value_changed")

signal value_changed()


