tool
extends Container

const SORT_DELAY = 0.02

class_name FlowContainer

export var use_minimum_size := false
export var gap := 0.0

#############
# overrides #
#############
func _notification(what):
	if what == NOTIFICATION_SORT_CHILDREN:
		__sort_children()

#################
# private stuff #
#################
func __sort_children():
	var pos := Vector2()
	var row_height := 0.0
	var vp_rect := get_viewport_rect()
	
	for child in get_children():
		if !child.visible || !child is Control:
			continue
		
		if use_minimum_size:
			child.rect_size = child.get_combined_minimum_size()
		
		if pos.x > 0.0 && pos.x + child.rect_size.x > rect_size.x:
			pos.x = 0.0
			pos.y += row_height + gap
			row_height = 0.0
		
		child.rect_position = pos
		
		row_height = max(row_height, child.rect_size.y)
		pos.x += child.rect_size.x + gap
	
	rect_min_size.y = pos.y + row_height
