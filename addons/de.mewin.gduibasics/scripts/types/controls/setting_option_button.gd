extends OptionButton

var settings = G.import("settings")

class_name SettingOptionButton

export var setting_name := ""
export(Array) var values := []
export(Array, String) var labels := []
export(Array, Texture) var icons := []

#############
# overrides #
#############
func _ready():
	__setup()
	
	connect("item_selected", self, "_on_item_selected")

#################
# private stuff #
#################
func __setup():
	clear()
	
	for i in range(values.size()):
		var value : String = values[i]
		var label : String = labels[i] if i < labels.size() else str(value)
		var icn : Texture = icons[i] if i < icons.size() else null
		
		add_item(label)
		set_item_icon(i, icn)
	
	if !setting_name:
		return
	
	var current_value = settings.get_value(setting_name)
	var current_idx = values.find(current_value)
	if current_idx >= 0:
		select(current_idx)

############
# handlers #
############
func _on_item_selected(idx : int):
	if setting_name:
		settings.set_value(setting_name, values[idx])
