extends Node

class_name FolderSync

enum DefaultFolder {
	HOME,
	ROOT,
	CUSTOM
}

var fsutil = G.import("fsutil")
var ux = G.import("ux")

export(Array, NodePath) var nodes = []
export(DefaultFolder) var default_folder = DefaultFolder.HOME
export(NodePath) var folder_up_action
export(NodePath) var folder_favourite_action
export var custom_default_folder := ""
var adapter : FileAdapter = LocalFileAdapter.new()

var current_folder : FileAdapter.FileEntry setget set_current_folder
var __folder_up_action : SimpleAction
var __folder_favourite_action : SimpleAction
var __nodes := []
var __locked := false

#############
# overrides #
#############
func _ready():
	__folder_up_action = __get_action(folder_up_action)
	__folder_favourite_action = __get_action(folder_favourite_action)
	
	if __folder_up_action:
		__folder_up_action.connect("applied", self, "_on_folder_up_action")
	
	if __folder_favourite_action:
		__folder_favourite_action.connect("applied", self, "_on_folder_favourite_action")
	
	var folder := "/"
	match default_folder:
		DefaultFolder.HOME:
			folder = fsutil.get_home_folder()
		DefaultFolder.CUSTOM:
			folder = custom_default_folder
	
	for path in nodes:
		var node := get_node(path)
		assert(node)
		if !node:
			continue
		node.adapter = adapter
		node.connect("current_folder_changed", self, "_on_node_current_folder_changed", [node])
		__nodes.append(node)
	
	var folder_entry = adapter._get_file(folder)
	if folder_entry && folder_entry._is_folder():
		set_current_folder(folder_entry)

################
# public stuff #
################
func set_current_folder(folder, ignore_node : Node = null):
	if __locked:
		return
	__locked = true
	
	for node in __nodes:
		if node != ignore_node:
			node.current_folder = folder
	
	__locked = false
	current_folder = folder
	
	if __folder_up_action:
		__folder_up_action.disabled = !folder || !folder._get_parent()
	if __folder_favourite_action:
		__folder_favourite_action.toggled = ux.is_favourite_place(current_folder)

func __get_action(node_path : NodePath) -> SimpleAction:
	var action_node = get_node_or_null(node_path)

	if action_node is SimpleAction:
		return action_node
	elif action_node is ActionReference:
		return action_node.get_action() as SimpleAction
	
	return null

############
# handlers #
############
func _on_node_current_folder_changed(source_node : Node):
	set_current_folder(source_node.current_folder, source_node)

func _on_folder_up_action():
	var parent = current_folder._get_parent()
	if parent:
		set_current_folder(parent)

func _on_folder_favourite_action():
	if ux.is_favourite_place(current_folder):
		ux.remove_favourite_place(current_folder)
		__folder_favourite_action.toggled = false
	else:
		ux.add_favourite_place(current_folder)
		__folder_favourite_action.toggled = true
