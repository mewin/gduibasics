extends Tree

class_name FileList

enum ViewMode {
	LIST,
	DETAILS
}
enum SpecialColumn {
	CHECKBOX
}
enum Checkable {
	NONE,
	FILES,
	FOLDERS,
	BOTH
}

class DetailsColumn:
	func _get_name() -> String:
		return ""
	
	func _get_value(entry) -> String:
		return ""
	
	func _get_expand() -> bool:
		return false
	
	func _get_min_width() -> float:
		return 10.0
	
	func _compare(entry0, entry1) -> bool:
		return _get_value(entry0) < _get_value(entry1)

class DetailsColumnName:
	extends DetailsColumn
	func _get_name() -> String:
		return tr("Name")
	func _get_value(entry) -> String:
		return entry._get_name()
	func _get_expand() -> bool:
		return true

class DetailsColumnType:
	extends DetailsColumn
	func _get_name() -> String:
		return tr("Type")
	func _get_value(entry) -> String:
		if entry._is_folder():
			return tr("Folder")
		else:
			return tr("File")
	func _get_min_width() -> float:
		return 100.0

class DetailsColumnSize:
	extends DetailsColumn
	func _get_name() -> String:
		return tr("Size")
	func _get_value(entry) -> String:
		if entry._is_folder():
			return ""
		else:
			return String.humanize_size(entry._get_size())
	func _get_min_width() -> float:
		return 100.0
	func _compare(entry0, entry1) -> bool:
		return entry0._get_size() < entry1._get_size()

class DetailsColumnModifiedTime:
	extends DetailsColumn
	func _get_name() -> String:
		return tr("Modified")
	func _get_value(entry) -> String:
		if entry._is_folder():
			return ""
		else:
			var format = G.import("format")
			return format.smart_format_unixtime(entry._get_modified_time())
	func _get_min_width() -> float:
		return 150.0
	func _compare(entry0, entry1) -> bool:
		return entry0._get_modified_time() < entry1._get_modified_time()

class __ParentFolderEntry:
	extends FileAdapter.FileEntry
	
	var real_entry
	
	func _init(real_entry_):
		real_entry = real_entry_
	
	func _get_name() -> String:
		return tr("<up>")
	
	func _is_folder() -> bool:
		return true

const CHECKBOX_COLUMN_WIDTH = 25

export(ViewMode) var view_mode = ViewMode.DETAILS setget _set_view_mode
export(Checkable) var checkable = Checkable.NONE setget _set_checkable
export var allow_navigation := true
export var show_up_entry := true
var adapter : FileAdapter setget _set_adapter
var filter : FileFilter setget _set_filter
var current_folder : FileAdapter.FileEntry setget _set_current_folder
var details_columns := [
	DetailsColumnName.new(),
	DetailsColumnType.new(),
	DetailsColumnSize.new(),
	DetailsColumnModifiedTime.new()
]
var __special_columns := []
var __sort_column := 0
var __sort_reverse := false

func _ready():
	hide_root = true
	select_mode = Tree.SELECT_ROW
	
	if adapter == null:
		adapter = LocalFileAdapter.new()
	
	if current_folder == null:
		current_folder = adapter._get_root()
	
	__update_entries()
	
	connect("item_activated", self, "_on_item_activated")
	connect("item_selected", self, "_on_item_selected")
	connect("column_title_pressed", self, "_on_column_title_pressed")

#################
# private stuff #
#################
func __compare_entries(entry0, entry1):
	if entry0._is_folder() != entry1._is_folder():
		return entry0._is_folder()
	var val = details_columns[__sort_column]._compare(entry0, entry1)
	if __sort_reverse:
		return !val
	return val

func __update_columns():
	__special_columns.clear()
	if checkable != Checkable.NONE:
		__special_columns.append(SpecialColumn.CHECKBOX)
	
	match view_mode:
		_: # DETAILS
			columns = __special_columns.size() + details_columns.size()
			set_column_titles_visible(true)
			for i in range(details_columns.size()):
				set_column_title(__special_columns.size() + i, details_columns[i]._get_name())
				set_column_expand(i, details_columns[i]._get_expand())
				set_column_min_width(i, details_columns[i]._get_min_width())

	for i in range(__special_columns.size()):
		set_column_title(i, "")
		set_column_expand(i, false)
		set_column_min_width(i, CHECKBOX_COLUMN_WIDTH)

func __entry_is_checkable(entry):
	match checkable:
		Checkable.BOTH:
			return true
		Checkable.FILES:
			return !entry._is_folder()
		Checkable.FOLDERS:
			return entry._is_folder()
		_:
			return false

func __update_entries():
	if !current_folder:
		return
	
	clear()
	create_item() # root
	
	__update_columns()
	if columns < 1:
		return
	
	var entries := current_folder._list_files()
	entries.sort_custom(self, "__compare_entries")
	
	if allow_navigation && show_up_entry:
		var parent_folder = current_folder._get_parent()
		if parent_folder:
			entries.push_front(__ParentFolderEntry.new(parent_folder))
	
	for entry in entries:
		if filter && !entry._is_folder() && !filter._accepts(entry):
			continue
		
		var itm = create_item()
		for i in range(__special_columns.size()):
			match __special_columns[i]:
				SpecialColumn.CHECKBOX:
					if __entry_is_checkable(entry):
						itm.set_cell_mode(i, TreeItem.CELL_MODE_CHECK)
		for i in range(details_columns.size()):
			itm.set_text(__special_columns.size() + i, details_columns[i]._get_value(entry))
		itm.set_icon(__special_columns.size(), adapter._get_icon(entry))
		itm.set_metadata(0, entry)

###########
# setters #
###########
func _set_view_mode(value):
	if value != view_mode:
		view_mode = value
		__update_entries()

func _set_adapter(value : FileAdapter):
	if value != adapter:
		adapter = value
		__update_entries()

func _set_filter(value : FileFilter):
	if value != filter:
		filter = value
		__update_entries()

func _set_current_folder(value : FileAdapter.FileEntry):
	if value != current_folder:
		current_folder = value
		emit_signal("current_folder_changed")
		__update_entries()

func _set_checkable(value):
	if value != checkable:
		checkable = value
		__update_entries()

############
# handlers #
############
func _on_item_activated():
	var selected := get_selected()
	if !selected:
		return
	var entry = selected.get_metadata(0)
	if !entry:
		return
	if allow_navigation && entry._is_folder():
		if entry is __ParentFolderEntry:
			_set_current_folder(entry.real_entry)
		else:
			_set_current_folder(entry)
	else:
		emit_signal("file_activated", entry)

func _on_item_selected():
	var selected := get_selected()
	if !selected:
		return
	var entry = selected.get_metadata(0)
	if !entry:
		return
	emit_signal("file_selected", entry)

func _on_column_title_pressed(idx : int):
	var sort_idx := idx - __special_columns.size()
	if sort_idx >= details_columns.size():
		return
	if sort_idx == __sort_column:
		__sort_reverse = !__sort_reverse
	else:
		__sort_column = sort_idx
		__sort_reverse = false
	__update_entries()

###########
# signals #
###########
signal current_folder_changed()
signal file_activated(file_entry)
signal file_selected(file_entry)
