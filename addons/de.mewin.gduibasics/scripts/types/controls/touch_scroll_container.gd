extends ScrollContainer

class_name TouchScrollContainer

export(float) var drag_threshold := 20.0

var __dragging := false
var __drag_start := Vector2()
var __drag_start_scroll := Vector2()
var __momentum := Vector2()

func _ready():
	set_process(false)
	
	get_h_scrollbar().modulate = Color.transparent
	get_v_scrollbar().modulate = Color.transparent

func _input(event):
	if event is InputEventMouseMotion && (event.button_mask & BUTTON_MASK_LEFT) && \
			(__dragging || get_global_rect().has_point(event.position)):
		if !__dragging:
			var dragged = event.position - __drag_start
			if abs(dragged.x) > drag_threshold \
					|| abs(dragged.y) > drag_threshold:
				__dragging = true
		else:
			__momentum = event.relative
			if scroll_horizontal_enabled:
				scroll_horizontal = __drag_start_scroll.x + __drag_start.x - event.position.x
			if scroll_vertical_enabled:
				scroll_vertical = __drag_start_scroll.y + __drag_start.y - event.position.y
	elif event is InputEventMouseButton && event.button_index == BUTTON_LEFT \
			&& get_global_rect().has_point(event.position):
		if !event.pressed:
			if __dragging:
				__dragging = false
				set_process(true) # process momentum
			else:
				__emulate_click(event)
		else:
			__drag_start = event.position
			__drag_start_scroll = Vector2(scroll_horizontal, scroll_vertical)
		get_tree().set_input_as_handled()

func _process(delta : float):
	__momentum *= 1.0 - 1.5 * delta
	if abs(__momentum.x) < 0.01 && abs(__momentum.y) < 0.01:
		set_process(false)
		return
	
	if scroll_horizontal_enabled:
		scroll_horizontal -= __momentum.x * 50.0 * delta
	
	if scroll_vertical_enabled:
		scroll_vertical -= __momentum.y * 50.0 * delta

func __emulate_click(orig_event : InputEventMouseButton):
	if get_child_count() < 1:
		return
	
	var child = get_child(0) as Control
	if !child:
		return
	
	set_process_input(false)
	var down_event : InputEventMouseButton = orig_event.duplicate()
	down_event.position = orig_event.global_position
	down_event.pressed = true
	Input.parse_input_event(down_event)
	
	yield(get_tree(), "idle_frame")
	
	var up_event : InputEventMouseButton = orig_event.duplicate()
	up_event.position = orig_event.global_position
	up_event.pressed = false
	Input.parse_input_event(up_event)
	set_process_input(true)
