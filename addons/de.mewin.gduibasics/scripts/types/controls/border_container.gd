tool
extends MarginContainer

class_name BorderContainer

enum __MouseMode {
	NONE,
	MOVING,
	RESIZE_L,
	RESIZE_R,
	RESIZE_T,
	RESIZE_B,
	RESIZE_TL,
	RESIZE_TR,
	RESIZE_BL,
	RESIZE_BR
}

export var resizable := true
export var show_title := false setget _set_show_title
export var title := "" setget _set_title
export var grab_everywhere := false
export(Texture) var title_image = null
export(StyleBox) var style_left setget _set_style_left
export(StyleBox) var style_right setget _set_style_right
export(StyleBox) var style_top setget _set_style_top
export(StyleBox) var style_bottom setget _set_style_bottom

var __mouse_mode = __MouseMode.NONE
var __mouse_down_pos : Vector2

#############
# overrides #
#############
func _gui_input(event):
	if event is InputEventMouseButton && event.pressed && event.button_index == BUTTON_LEFT:
		if resizable:
			__mouse_mode = __resize_mode(event.position)
			if __mouse_mode != __MouseMode.NONE:
				__mouse_down_pos = _win_get_mouse_position() - _win_get_position()
				if __mouse_mode == __MouseMode.RESIZE_B || __mouse_mode == __MouseMode.RESIZE_BL || __mouse_mode == __MouseMode.RESIZE_BR:
					__mouse_down_pos.y -= _win_get_size().y
				if __mouse_mode == __MouseMode.RESIZE_R || __mouse_mode == __MouseMode.RESIZE_TR || __mouse_mode == __MouseMode.RESIZE_BR:
					__mouse_down_pos.x -= _win_get_size().x
				set_process(true)
				return
		if grab_everywhere || event.position.y < __get_margin("top"):
			__mouse_mode = __MouseMode.MOVING
			__mouse_down_pos = _win_get_mouse_position() - _win_get_position()
			set_process(true)
			accept_event()
	elif event is InputEventMouseMotion && resizable:
		match __resize_mode(event.position):
			__MouseMode.RESIZE_L, __MouseMode.RESIZE_R:
				mouse_default_cursor_shape = CURSOR_HSIZE
			__MouseMode.RESIZE_T, __MouseMode.RESIZE_B:
				mouse_default_cursor_shape = CURSOR_VSIZE
			__MouseMode.RESIZE_TL, __MouseMode.RESIZE_BR:
				mouse_default_cursor_shape = CURSOR_FDIAGSIZE
			__MouseMode.RESIZE_TR, __MouseMode.RESIZE_BL:
				mouse_default_cursor_shape = CURSOR_BDIAGSIZE
			_:
				mouse_default_cursor_shape = CURSOR_ARROW

func _process(delta):
	if !Input.is_mouse_button_pressed(BUTTON_LEFT):
		__mouse_mode = __MouseMode.NONE
	
	if __mouse_mode == __MouseMode.NONE:
		set_process(false)
		return
	
	if __mouse_mode == __MouseMode.MOVING:
		_win_set_position(_win_get_mouse_position() - __mouse_down_pos)
		return
	
	var win_pos := _win_get_position()
	var win_size := _win_get_size()
	var win_min_size := _win_get_minimum_size()
	var mouse_pos := _win_get_mouse_position()
	
	if __mouse_mode == __MouseMode.RESIZE_L || __mouse_mode == __MouseMode.RESIZE_TL || __mouse_mode == __MouseMode.RESIZE_BL:
		var global_right := win_pos.x + win_size.x
		var x_pos := mouse_pos.x - __mouse_down_pos.x
		x_pos = min(x_pos, win_pos.x + win_size.x - win_min_size.x)
		win_pos.x = x_pos
		win_size.x = global_right - win_pos.x
	elif __mouse_mode == __MouseMode.RESIZE_R || __mouse_mode == __MouseMode.RESIZE_TR || __mouse_mode == __MouseMode.RESIZE_BR:
		var x_pos := mouse_pos.x - __mouse_down_pos.x
		win_size.x = x_pos - win_pos.x
		win_size.x = max(win_size.x, win_min_size.x)
		
	if __mouse_mode == __MouseMode.RESIZE_T || __mouse_mode == __MouseMode.RESIZE_TL || __mouse_mode == __MouseMode.RESIZE_TR:
		var global_bottom := win_pos.y + win_size.y
		var y_pos := mouse_pos.y - __mouse_down_pos.y
		y_pos = min(y_pos, win_pos.y + win_size.y - win_min_size.y)
		win_pos.y = y_pos
		win_size.y = global_bottom - win_pos.y
	elif __mouse_mode == __MouseMode.RESIZE_B || __mouse_mode == __MouseMode.RESIZE_BL || __mouse_mode == __MouseMode.RESIZE_BR:
		var y_pos := mouse_pos.y - __mouse_down_pos.y
		win_size.y = y_pos - win_pos.y
		win_size.y = max(win_size.y, win_min_size.y)
	
	_win_set_position(win_pos)
	_win_set_size(win_size)

func _draw():
	var marg_left = __get_margin("left")
	var marg_right = __get_margin("right")
	var marg_top = __get_margin("top")
	var marg_bottom = __get_margin("bottom")
	
	if show_title:
		var title_height := 0
		if title_image:
			# title_height = title_image.get_height()
			title_height = 0.0
		else:
			var font := get_font("font", "Label")
			title_height = font.get_height() + font.get_descent()
		if marg_top < title_height:
			marg_top = title_height
			set("custom_constants/margin_top", marg_top)
	
	if style_left:
		var min_size = style_left.get_minimum_size()
		min_size.x = max(min_size.x, marg_left)
		draw_style_box(style_left, Rect2(Vector2(), Vector2(min_size.x, rect_size.y)))
		
	if style_right:
		var min_size = style_right.get_minimum_size()
		min_size.x = max(min_size.x, marg_right)
		draw_style_box(style_right, Rect2(Vector2(rect_size.x - min_size.x, 0.0), Vector2(min_size.x, rect_size.y)))
		
	if style_top:
		var min_size = style_top.get_minimum_size()
		min_size.y = max(min_size.y, marg_top)
		draw_style_box(style_top, Rect2(Vector2(), Vector2(rect_size.x, min_size.y)))
	
	if style_bottom:
		var min_size = style_bottom.get_minimum_size()
		min_size.y = max(min_size.y, marg_bottom)
		draw_style_box(style_bottom, Rect2(Vector2(0.0, rect_size.y - min_size.y), Vector2(rect_size.x, min_size.y)))
	
	if show_title:
		if title_image:
			draw_texture(title_image, Vector2(0.5 * (rect_size.x - title_image.get_width()), 0.0))
		else:
			var font := get_font("font", "Label")
			var string_size := font.get_string_size(title)
			draw_string(font, Vector2(0.5 * (rect_size.x - string_size.x), string_size.y), title)

################
# overridables #
################
func _win_get_position() -> Vector2:
	return get_parent().rect_global_position

func _win_get_size() -> Vector2:
	return get_parent().rect_size

func _win_get_mouse_position() -> Vector2:
	return get_viewport().get_mouse_position()

func _win_get_minimum_size() -> Vector2:
	return get_parent().rect_min_size

func _win_set_position(pos : Vector2):
	get_parent().rect_global_position = pos

func _win_set_size(size : Vector2):
	get_parent().rect_size = size

#################
# private stuff #
#################
func __get_margin(what : String) -> int:
	var marg = get("custom_constants/margin_%s" % what)
	if !marg:
		return 0
	return marg

func __resize_mode(cursor_pos : Vector2):
	var resize_left := false
	var resize_right := false
	var resize_top := false
	var resize_bottom := false
	
	if cursor_pos.y < __get_margin("bottom"):
		resize_top = true
	elif cursor_pos.y > rect_size.y - __get_margin("bottom"):
		resize_bottom = true			
	if cursor_pos.x < __get_margin("left"):
		resize_left = true
	elif cursor_pos.x > rect_size.x - __get_margin("right"):
		resize_right = true
	
	if resize_top:
		if resize_left:
			return __MouseMode.RESIZE_TL
		elif resize_right:
			return __MouseMode.RESIZE_TR
		else:
			return __MouseMode.RESIZE_T
	elif resize_bottom:
		if resize_left:
			return __MouseMode.RESIZE_BL
		elif resize_right:
			return __MouseMode.RESIZE_BR
		else:
			return __MouseMode.RESIZE_B
	elif resize_left:
		return __MouseMode.RESIZE_L
	elif resize_right:
		return __MouseMode.RESIZE_R
	else:
		return __MouseMode.NONE

###########
# setters #
###########
func _set_show_title(val : bool):
	if val != show_title:
		show_title = val
		update()

func _set_title(val : String):
	if val != title:
		title = val
		update()

func _set_style_left(val : StyleBox):
	if val != style_left:
		style_left = val
		update()

func _set_style_right(val : StyleBox):
	if val != style_right:
		style_right = val
		update()

func _set_style_top(val : StyleBox):
	if val != style_top:
		style_top = val
		update()

func _set_style_bottom(val : StyleBox):
	if val != style_bottom:
		style_bottom = val
		update()
