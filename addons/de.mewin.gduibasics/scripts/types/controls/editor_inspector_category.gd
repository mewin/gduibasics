extends Control

class_name GDUIBEditorInspectorCategory

var label := "" setget _set_label
var icon : Texture setget _set_icon
onready var bg_color := get_color("prop_category", "Editor") setget _set_bg_color

func _ready():
	update()

func _draw():
	draw_rect(Rect2(Vector2(), get_size()), bg_color)
	var font := get_font("font", "Tree")

	var hs := get_constant("hseparation", "Tree")

	var w := font.get_string_size(label).x
	if icon:
		w += hs + icon.get_width()

	var ofs := (get_size().x - w) / 2

	if icon:
		draw_texture(icon, Vector2(ofs, (get_size().y - icon.get_height()) / 2).floor())
		ofs += hs + icon.get_width()

	var color := get_color("font_color", "Tree");
	draw_string(font, Vector2(ofs, font.get_ascent() + (get_size().y - font.get_height()) / 2).floor(), label, color, get_size().x)

func _get_minimum_size() -> Vector2:
	var ms : Vector2
	var font := get_font("font", "Tree");

	ms.x = 1
	ms.y = font.get_height()
#	if (icon.is_valid()) {
#		ms.height = MAX(icon->get_height(), ms.height);
#	}
	ms.y += get_constant("vseparation", "Tree")

	return ms

func _set_label(value : String):
	if value != label:
		label = value
		update()

func _set_icon(value : Texture):
	if value != icon:
		icon = value
		update()

func _set_bg_color(value : Color):
	if value != bg_color:
		bg_color = value
		update()
