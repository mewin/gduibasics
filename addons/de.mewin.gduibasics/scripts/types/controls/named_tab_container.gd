extends TabContainer

class_name NamedTabContainer

export(Array, String) var headers := []
export(Array, Texture) var icons := []

func _ready():
	for i in range(headers.size()):
		if i >= get_child_count():
			break
		set_tab_title(i, headers[i])
	for i in range(icons.size()):
		if i >= get_child_count():
			break
		set_tab_icon(i, icons[i])
