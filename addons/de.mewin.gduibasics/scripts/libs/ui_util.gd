extends Node

class_name GDBUIUitility

static func uncollapse_tree_item(tree_item : TreeItem):
	while tree_item:
		tree_item.collapsed = false
		tree_item = tree_item.get_parent()

static func find_scene_tree_editor(root : Node = null) -> Node:
	if not root:
		root = (Engine.get_main_loop() as SceneTree).root
	if root.get_class() == "SceneTreeEditor":
		return root
	for child in root.get_children():
		var scene_tree_editor := find_scene_tree_editor(child)
		if scene_tree_editor:
			return scene_tree_editor
	return null
