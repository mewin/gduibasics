# various UX stuff
extends Node

var settings = G.import("settings")

################
# public stuff #
################
func get_recent_places() -> Array:
	return settings.get_value(C.SETTING_RECENT_PLACES, [])

func add_recent_place(place):
	if place is FileAdapter.FileEntry:
		add_recent_place(place._get_path())
		return
	
	assert(place is String)
	var recent_places : Array = settings.get_value(C.SETTING_RECENT_PLACES, [])
	recent_places.erase(place)
	recent_places.push_front(place)
	settings.set_value(C.SETTING_RECENT_PLACES, recent_places)
	
	emit_signal("recent_places_changed")

func get_favourite_places() -> Array:
	return settings.get_value(C.SETTING_FAVOURITE_PLACES, [])

func is_favourite_place(place):
	if place is FileAdapter.FileEntry:
		return is_favourite_place(place._get_path())
	
	assert(place is String)
	return settings.array_has_value(C.SETTING_FAVOURITE_PLACES, place)

func add_favourite_place(place):
	if place is FileAdapter.FileEntry:
		add_favourite_place(place._get_path())
		return
	
	assert(place is String)
	var places = settings.get_value(C.SETTING_FAVOURITE_PLACES, [])
	if !places.has(place):
		places.append(place)
		settings.set_value(C.SETTING_FAVOURITE_PLACES, places)
		
		emit_signal("favourite_places_changed")

func remove_favourite_place(place):
	if place is FileAdapter.FileEntry:
		remove_favourite_place(place._get_path())
		return
	
	assert(place is String)
	var places = settings.get_value(C.SETTING_FAVOURITE_PLACES, [])
	if places.has(place):
		places.erase(place)
		settings.set_value(C.SETTING_FAVOURITE_PLACES, places)
		
		emit_signal("favourite_places_changed")

###########
# signals #
###########
signal recent_places_changed()
signal favourite_places_changed()
